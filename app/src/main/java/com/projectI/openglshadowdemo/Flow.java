package com.projectI.openglshadowdemo;

import android.content.Context;

/**
 * Created by Kirill on 03.12.14.
 */
public class Flow {
    private float mSpeed;
    private Triangle mObject;
    private float mX;
    public static final float SIZE = 20.0f;

    public Flow(float x, float z, int speed, Context context) {
        mSpeed = speed;
        mSpeed += 1.0f;
        mSpeed /= 10.0f;
        mObject = new Triangle(0.1f, 0.1f, 0.1f, 0, new float[] {x * SIZE - 12.7f, -0.5f, z}, new float[] {0.0f, 0.0f, 0.999f, 0.99f});
        mX = x * SIZE;
    }

    public float getTranslation() {
        if(mX >= SIZE) {
            mX = 0;
            return -SIZE;
        } else {
            mX += mSpeed;
            return mX;
        }
    }

    public Triangle getObject() {
        return mObject;
    }
}
