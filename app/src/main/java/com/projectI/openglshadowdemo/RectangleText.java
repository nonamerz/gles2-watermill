package com.projectI.openglshadowdemo;

import android.content.Context;
import android.opengl.GLES20;

import com.projectI.openglshadowdemo.common.RenderConstants;
import com.projectI.openglshadowdemo.common.TextureHelper;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Simple cube in openGL
 *
 */
public class RectangleText {
    private final float[] mPosition;

    /** Store our model data in a float buffer. */
    private final FloatBuffer mCubeTextureCoordinates;
    private final FloatBuffer mCubePositions;
    private final FloatBuffer mCubeNormals;
    private final int mTextureDataHandle;

    /** This will be used to pass in model texture coordinate information. */

    /** Size of the texture coordinate data in elements. */
    private final int mTextureCoordinateDataSize = 2;


    public float[] getPosition() {
        return mPosition;
    }
    // 6 sides * 2 triangles * 3 vertices * 3 coordinates





    // X, Y, Z
    // The normal is used in light calculations and is a vector which points
    // orthogonal to the plane of the surface. For a cube model, the normals
    // should be orthogonal to the points of each face.
    final float[] cubeNormalData =
            {
                    // Front face
                    0.0f, 0.0f, 1.0f,
                    0.0f, 0.0f, 1.0f,
                    0.0f, 0.0f, 1.0f,
                    0.0f, 0.0f, 1.0f,
                    0.0f, 0.0f, 1.0f,
                    0.0f, 0.0f, 1.0f,

                    // Right face
                    1.0f, 0.0f, 0.0f,
                    1.0f, 0.0f, 0.0f,
                    1.0f, 0.0f, 0.0f,
                    1.0f, 0.0f, 0.0f,
                    1.0f, 0.0f, 0.0f,
                    1.0f, 0.0f, 0.0f,

                    // Back face
                    0.0f, 0.0f, -1.0f,
                    0.0f, 0.0f, -1.0f,
                    0.0f, 0.0f, -1.0f,
                    0.0f, 0.0f, -1.0f,
                    0.0f, 0.0f, -1.0f,
                    0.0f, 0.0f, -1.0f,

                    // Left face
                    -1.0f, 0.0f, 0.0f,
                    -1.0f, 0.0f, 0.0f,
                    -1.0f, 0.0f, 0.0f,
                    -1.0f, 0.0f, 0.0f,
                    -1.0f, 0.0f, 0.0f,
                    -1.0f, 0.0f, 0.0f,

                    // Top face
                    0.0f, 1.0f, 0.0f,
                    0.0f, 1.0f, 0.0f,
                    0.0f, 1.0f, 0.0f,
                    0.0f, 1.0f, 0.0f,
                    0.0f, 1.0f, 0.0f,
                    0.0f, 1.0f, 0.0f,

                    // Bottom face
                    0.0f, -1.0f, 0.0f,
                    0.0f, -1.0f, 0.0f,
                    0.0f, -1.0f, 0.0f,
                    0.0f, -1.0f, 0.0f,
                    0.0f, -1.0f, 0.0f,
                    0.0f, -1.0f, 0.0f
            };

    // S, T (or X, Y)
    // Texture coordinate data.
    // Because images have a Y axis pointing downward (values increase as you move down the image) while
    // OpenGL has a Y axis pointing upward, we adjust for that here by flipping the Y axis.
    // What's more is that the texture coordinates are the same for every face.
    final float[] cubeTextureCoordinateData =
            {
                    // Front face
                    0.0f, 0.0f,
                    0.0f, 1.0f,
                    1.0f, 0.0f,
                    0.0f, 1.0f,
                    1.0f, 1.0f,
                    1.0f, 0.0f,

                    // Right face
                    0.0f, 0.0f,
                    0.0f, 1.0f,
                    1.0f, 0.0f,
                    0.0f, 1.0f,
                    1.0f, 1.0f,
                    1.0f, 0.0f,

                    // Back face
                    0.0f, 0.0f,
                    0.0f, 1.0f,
                    1.0f, 0.0f,
                    0.0f, 1.0f,
                    1.0f, 1.0f,
                    1.0f, 0.0f,

                    // Left face
                    0.0f, 0.0f,
                    0.0f, 1.0f,
                    1.0f, 0.0f,
                    0.0f, 1.0f,
                    1.0f, 1.0f,
                    1.0f, 0.0f,

                    // Top face
                    0.0f, 0.0f,
                    0.0f, 1.0f,
                    1.0f, 0.0f,
                    0.0f, 1.0f,
                    1.0f, 1.0f,
                    1.0f, 0.0f,

                    // Bottom face
                    0.0f, 0.0f,
                    0.0f, 1.0f,
                    1.0f, 0.0f,
                    0.0f, 1.0f,
                    1.0f, 1.0f,
                    1.0f, 0.0f
            };

    //size of color data array: 6 sides * 2 triangles * 3 points * 4 color (RGBA) values
	float[] cubeColorData = new float[12*3*4];

	/**
	 * Create a new cube with specified center position, size and color
	 */
	public RectangleText(float x, float y, float z, float zOffset, float[] position, float[] color, Context context, int resId) {
		//set color data
		for (int v = 0; v < 12*3 ; v++){
			cubeColorData[4*v+0] = color[0];
			cubeColorData[4*v+1] = color[1];
			cubeColorData[4*v+2] = color[2];
			cubeColorData[4*v+3] = color[3];
		}

        float[] cubePositionData = getPositionData(x, y, z, zOffset);
        for (int j = 0; j < 36; j++) {
            cubePositionData[3*j] = cubePositionData[3*j] + position[0];
            cubePositionData[3*j + 1] = cubePositionData[3*j + 1] + position[1];
            cubePositionData[3*j + 2] = cubePositionData[3*j + 2] + position[2];
        }
        mPosition = position;

        // Initialize the buffers.
        mCubePositions = ByteBuffer.allocateDirect(cubePositionData.length * RenderConstants.FLOAT_SIZE_IN_BYTES)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mCubePositions.put(cubePositionData).position(0);

        mCubeNormals = ByteBuffer.allocateDirect(cubeNormalData.length * RenderConstants.FLOAT_SIZE_IN_BYTES)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mCubeNormals.put(cubeNormalData).position(0);

        mCubeTextureCoordinates = ByteBuffer.allocateDirect(cubeTextureCoordinateData.length * RenderConstants.FLOAT_SIZE_IN_BYTES)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mCubeTextureCoordinates.put(cubeTextureCoordinateData).position(0);

        mTextureDataHandle = TextureHelper.loadTexture(context, resId);
        GLES20.glGenerateMipmap(GLES20.GL_TEXTURE_2D);


	}

    private float[] getPositionData(float x, float y, float z, float offset) {
        return new float[]
                {
                        // In OpenGL counter-clockwise winding is default. This means that when we look at a triangle,
                        // if the points are counter-clockwise we are looking at the "front". If not we are looking at
                        // the back. OpenGL has an optimization where all back-facing triangles are culled, since they
                        // usually represent the backside of an object and aren't visible anyways.

                        // Front face
                        0.0f,	y,		z + offset,		//011
                        0.0f,	0.0f,	z + offset,		//001
                        x,		y,		z + offset,		//111
                        0.0f,	0.0f,	z + offset,		//001
                        x,		0.0f,	z + offset,		//101
                        x,		y,		z + offset,		//111

                        // Right face
                        x,		y,		z + offset,		//111
                        x,		0.0f,	z + offset,		//101
                        x,		y,		0 + offset,		//110
                        x,		0.0f,	z + offset,		//101
                        x,		0.0f,	0 + offset,		//100
                        x,		y,		0 + offset,		//110

                        // Back face
                        x,		y,		0 + offset,		//110
                        x,		0.0f,	0 + offset,		//100
                        0.0f,	y,		0 + offset,		//010
                        x,		0.0f,	0 + offset,		//100
                        0.0f,	0.0f,	0 + offset,		//000
                        0.0f,	y,		0 + offset,		//010

                        // Left face
                        0.0f,	y,		0 + offset,		//010
                        0.0f,	0.0f,	0 + offset,		//000
                        0.0f,	y,		z + offset,		//011
                        0.0f,	0.0f,	0 + offset,		//000
                        0.0f,	0.0f,	z + offset,		//001
                        0.0f,	y,		z + offset,		//011

                        // Top face
                        0.0f,	y,		0 + offset,		//010
                        0.0f,	y,		z + offset,		//011
                        x,		y,		0 + offset,		//110
                        0.0f,	y,		z + offset,		//011
                        x,		y,		z + offset,		//111
                        x,		y,		0 + offset,		//110

                        // Bottom face
                        x,		y,		0 + offset,		//110
                        x,		0.0f,	z + offset,		//101
                        0.0f,	0.0f,	0 + offset,		//000
                        x,		0.0f,	z + offset,		//101
                        0.0f,	0.0f,	z + offset,		//001
                        0.0f,	0.0f,	0 + offset,		//000
                };
    }

    public void render(int positionAttribute, int normalAttribute, int textureAttribute, int textureUniforim, boolean onlyPosition) {
		
		render(positionAttribute, normalAttribute, textureAttribute, textureUniforim, onlyPosition, false);
	}
    public void render(int positionAttribute, int normalAttribute, int textureAttribute, int textureUniforim, boolean onlyPosition, boolean isTransparent) {

        mCubePositions.position(0);
        GLES20.glVertexAttribPointer(positionAttribute, 3, GLES20.GL_FLOAT, false, 0, mCubePositions);
        GLES20.glEnableVertexAttribArray(positionAttribute);

        if (!onlyPosition) {
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle);
            GLES20.glUniform1i(textureUniforim, 0);

            // Pass in the texture coordinate information
            mCubeTextureCoordinates.position(0);
            GLES20.glVertexAttribPointer(textureAttribute, mTextureCoordinateDataSize, GLES20.GL_FLOAT, false, 0, mCubeTextureCoordinates);
            GLES20.glEnableVertexAttribArray(textureAttribute);

            // Pass in the normal information
            mCubeNormals.position(0);
            GLES20.glVertexAttribPointer(normalAttribute, 3, GLES20.GL_FLOAT, false, 0, mCubeNormals);
            GLES20.glEnableVertexAttribArray(normalAttribute);
        }
        if(isTransparent) {
            // Draw the cube.
            GLES20.glDepthMask(false);
            GLES20.glEnable(GLES20.GL_BLEND);
            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
            GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 36);
            GLES20.glDisable(GLES20.GL_BLEND);
            GLES20.glDepthMask(true);
        } else {
            GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 36);
        }
    }


}