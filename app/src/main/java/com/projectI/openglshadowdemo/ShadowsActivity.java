package com.projectI.openglshadowdemo;

import android.app.Activity;
import android.os.Bundle;

public class ShadowsActivity extends Activity {

    private ShadowsGLSurfaceView mGLView;
    private ShadowsRenderer renderer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Create a GLSurfaceView instance and set it
        // as the ContentView for this Activity
        mGLView = new ShadowsGLSurfaceView(this);
        
        // Create an OpenGL ES 2.0 context.
        mGLView.setEGLContextClientVersion(2);
        
		renderer = new ShadowsRenderer(this);
		mGLView.setRenderer(renderer);
        
        setContentView(mGLView);
        
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGLView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGLView.onResume();
    }


}
