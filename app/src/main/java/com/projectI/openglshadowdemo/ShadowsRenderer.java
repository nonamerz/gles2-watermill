package com.projectI.openglshadowdemo;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.SystemClock;
import android.util.Log;

import com.projectI.openglshadowdemo.common.FPSCounter;
import com.projectI.openglshadowdemo.common.RenderConstants;
import com.projectI.openglshadowdemo.common.RenderProgram;

import java.util.Random;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class ShadowsRenderer implements GLSurfaceView.Renderer {

    private static final String TAG = "ShadowsRenderer";
    private static final int FLOW_COUNT = 300;
    private static final int SHADOW_MAP_RATIO = 1;

    private final ShadowsActivity mShadowsActivity;

    private FPSCounter mFPSCounter;
    
    /**
	 * Handles to vertex and fragment shader programs
	 */
	private RenderProgram mTextProgram;


	
	/**
	 * The vertex and fragment shader to render depth map
	 */
	private RenderProgram mDepthMapProgram;
	
	private int mActiveProgram;
	
    private final float[] mMVPMatrix = new float[16];
    private final float[] mMVMatrix = new float[16];
    private final float[] mNormalMatrix = new float[16];
    private final float[] mProjectionMatrix = new float[16];
    private final float[] mViewMatrix = new float[16];
    private final float[] mModelMatrix = new float[16];

    private final float[] mCubeRotation = new float[16];
    
    /**
     * MVP matrix used at rendering shadow map for stationary objects
     */
    private final float[] mLightMvpMatrix_staticShapes = new float[16];
    
    /**
     * MVP matrix used at rendering shadow map for the big cube in the center
     */
    private final float[] mLightMvpMatrix_dynamicShapes = new float[16];
    
    /**
     * Projection matrix from point of light source
     */
    private final float[] mLightProjectionMatrix = new float[16];
    
    /**
     * View matrix of light source
     */
    private final float[] mLightViewMatrix = new float[16];
    
    /**
     * Position of light source in eye space
     */
    private final float[] mLightPosInEyeSpace = new float[16];
    
    /**
     * Light source position in model space
     */
    private final float[] mLightPosModel = new float []
    		{-5.0f, 15.0f, 6.0f, 1.0f};
    
    private float[] mActualLightPosition = new float[4];
    
    /**
     * Current X,Y axis rotation of center cube
     */
    private float mRotationX;
    private float mRotationY;

    /**
     * Current display sizes
     */
	private int mDisplayWidth;
	private int mDisplayHeight;
	
	/**
	 * Current shadow map sizes
	 */
	private int mShadowMapWidth;
	private int mShadowMapHeight;
    

	int[] fboId;
	int[] depthTextureId;
	int[] renderTextureId;
	
	// Uniform locations for scene render program
	private int scene_mvpMatrixUniform;
	private int scene_mvMatrixUniform;
	private int scene_lightPosUniform;
	private int scene_textureShadowUniform;
	
	// Uniform locations for shadow render program
	private int shadow_mvpMatrixUniform;
	
	// Shader program attribute locations
	private int scene_positionAttribute;
	private int scene_normalAttribute;

	private int SHADOWscene_colorAttribute;
    private int SHADOWscene_schadowProjMatrixUniform;
    private int SHADOWscene_normalMatrixUniform;

	private int shadow_positionAttribute;

    private Rectangle mBuilding;
    private RectangleText mBuildingLittle;
    private Rectangle mLandRight;
    private Rectangle mLandLeft;
    private RectangleText mWater;
    private Rectangle mRail;
    private TriangleText mRoof;
    private RectangleText[] mRectangles;
    private Flow[] mFlows;


    private static float mMillAngle = 0;
    private static float mMillSpeed = 3.0f;
    private int scene_textureCoord;
    private RenderProgram mShadowProgram;
    private int mShadowProgramInt;
    private int scene_textureUniform;

    public ShadowsRenderer(final ShadowsActivity shadowsActivity) {
		mShadowsActivity = shadowsActivity;
	}
	
    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
		mFPSCounter = new FPSCounter();

        //Set the background frame color
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
        //Enable depth testing
		GLES20.glEnable(GLES20.GL_DEPTH_TEST);
		
		GLES20.glEnable(GLES20.GL_CULL_FACE);
        mRoof = new TriangleText(3.2f, 2.0f, 3.2f, 0, new float[] {-2.1f, 2.6f, -12.1f}, new float[] {0.46f, 0.53f, 0.6f, 1.0f}, mShadowsActivity, R.drawable.rooftexts);
        mBuilding = new Rectangle(3.0f, 3.0f, 3.0f, 0, new float[] {-2.0f, -0.4f, -12.0f}, new float[] {0.46f, 0.53f, 0.6f, 1.0f});
        mBuildingLittle = new RectangleText(1.0f, 1.0f, 1.0f, 0, new float[] {-0.5f, -0.4f, -5.4f}, new float[] {0.46f, 0.53f, 0.6f, 1.0f}, mShadowsActivity, R.drawable.bricks);
        mLandRight = new Rectangle(25.0f, 5.0f, 25.0f, 0, new float[] {-12.7f, -5.3f, -34.0f}, new float[] {0.0f, 0.392f, 0.0f, 1.0f});
        mLandLeft = new Rectangle(25.0f, 5.0f, 25.0f, 0, new float[] {-12.7f, -5.3f, -5.5f}, new float[] {0.0f, 0.392f, 0.0f, 1.0f});
        mWater = new RectangleText(25.0f, 4.9f, 3.5f, 0, new float[] {-12.7f, -5.3f, -9f}, new float[] {0.0f, 0.0f, 1.0f, 0.5f}, mShadowsActivity, R.drawable.water);
        mRail = new Rectangle(0.05f, 0.05f, 4.0f, 0, new float[] {-0.025f, 0.0f, -9.25f}, new float[] {0.0f, 0.0f, 0.0f, 1.0f});
        mRectangles = new RectangleText[8];
        for (int i = 0; i < 8; i++) {
            mRectangles[i] = new RectangleText(1.0f, 2.0f, 0.05f, 0, new float[] {-0.0f, +6.5f, -0.0f}, new float[] {0.46f, 0.53f, 0.6f, 1.0f}, mShadowsActivity, R.drawable.wood);
        }
        Random random = new Random();
        mFlows = new Flow[FLOW_COUNT];
        for(int i = 0; i < FLOW_COUNT; i++) {
            mFlows[i] = new Flow(random.nextFloat(), random.nextFloat() * (-9f + 5.5f) - 5.5f, random.nextInt(5), mShadowsActivity);
        }


        //Load shaders and create program used by OpenGL for rendering
        mTextProgram = new RenderProgram(R.raw.v_own, R.raw.f_own, mShadowsActivity);
        mDepthMapProgram = new RenderProgram(R.raw.v_depth_map, R.raw.f_depth_map, mShadowsActivity);


        mActiveProgram = mTextProgram.getProgram();

    }

    /**
	 * Sets up the framebuffer and renderbuffer to render to texture
	 */
	public void generateShadowFBO()
	{
		mShadowMapWidth = Math.round(mDisplayWidth * SHADOW_MAP_RATIO);
		mShadowMapHeight = Math.round(mDisplayHeight * SHADOW_MAP_RATIO);
		
		fboId = new int[1];
		depthTextureId = new int[1];
		renderTextureId = new int[1];

		// create a framebuffer object
		GLES20.glGenFramebuffers(1, fboId, 0);
		
		// create render buffer and bind 16-bit depth buffer
		GLES20.glGenRenderbuffers(1, depthTextureId, 0);
		GLES20.glBindRenderbuffer(GLES20.GL_RENDERBUFFER, depthTextureId[0]);
		GLES20.glRenderbufferStorage(GLES20.GL_RENDERBUFFER, GLES20.GL_DEPTH_COMPONENT16, mShadowMapWidth, mShadowMapHeight);
		
		// Try to use a texture depth component
		GLES20.glGenTextures(1, renderTextureId, 0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, renderTextureId[0]);
		
		// GL_LINEAR does not make sense for depth texture. However, next tutorial shows usage of GL_LINEAR and PCF. Using GL_NEAREST
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
		
		// Remove artifact on the edges of the shadowmap
		GLES20.glTexParameteri( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE );
		GLES20.glTexParameteri( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE );
		
		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, fboId[0]);







        GLES20.glTexImage2D( GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, mShadowMapWidth, mShadowMapHeight, 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, null);

        // specify texture as color attachment
        GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, renderTextureId[0], 0);

        // attach the texture to FBO depth attachment point
        // (not supported with gl_texture_2d)
        GLES20.glFramebufferRenderbuffer(GLES20.GL_FRAMEBUFFER, GLES20.GL_DEPTH_ATTACHMENT, GLES20.GL_RENDERBUFFER, depthTextureId[0]);
		
		// check FBO status
		int FBOstatus = GLES20.glCheckFramebufferStatus(GLES20.GL_FRAMEBUFFER);
		if(FBOstatus != GLES20.GL_FRAMEBUFFER_COMPLETE) {
			Log.e(TAG, "GL_FRAMEBUFFER_COMPLETE failed, CANNOT use FBO");
			throw new RuntimeException("GL_FRAMEBUFFER_COMPLETE failed, CANNOT use FBO");
		}
	}
    
    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {
    	mDisplayWidth = width;
		mDisplayHeight = height;
    	
    	// Adjust the viewport based on geometry changes,
        // such as screen rotation
        GLES20.glViewport(0, 0, mDisplayWidth, mDisplayHeight);

        // Generate buffer where depth values are saved for shadow calculation
        generateShadowFBO();
        
        float ratio = (float) mDisplayWidth / mDisplayHeight;

        // this projection matrix is applied at rendering scene
        // in the onDrawFrame() method
        float bottom = -1.0f;
        float top = 1.0f;
        float near = 1.0f;
        float far = 100.0f;
        
        Matrix.frustumM(mProjectionMatrix, 0, -ratio, ratio, bottom, top, near, far);
        
        // this projection matrix is used at rendering shadow map
        Matrix.frustumM(mLightProjectionMatrix, 0, -1.1f*ratio, 1.1f*ratio, 1.1f*bottom, 1.1f*top, near, far);
        //Matrix.frustumM(mLightProjectionMatrix, 0, -ratio, ratio, bottom, top, near, far);
    }

    @Override
    public void onDrawFrame(GL10 unused) {
    	// Write FPS information to console
    	mFPSCounter.logFrame();

        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        float offset = -10;
        float zoom = getRotationY() / 45;
        zoom = zoom < -40 ? -40 : zoom > -1 ? -1 : zoom;
        //-40 to 0
        Matrix.setLookAtM(mViewMatrix, 0,
                //eyeX, eyeY, eyeZ,
                (float)(zoom * Math.cos(getRotationX() / 45)), 5, (float)(zoom * Math.sin(getRotationX() / 45)) + offset,
                //lookX, lookY, lookZ,
                0, 0f, offset,
                //upX, upY, upZ
                0, 1f, 0);
    	
		// Set program handles for cube drawing.


        SHADOWscene_normalMatrixUniform = GLES20.glGetUniformLocation(mActiveProgram, RenderConstants.NORMAL_MATRIX_UNIFORM);
        SHADOWscene_schadowProjMatrixUniform = GLES20.glGetUniformLocation(mActiveProgram, RenderConstants.SHADOW_PROJ_MATRIX);
        SHADOWscene_colorAttribute = GLES20.glGetAttribLocation(mActiveProgram, RenderConstants.COLOR_ATTRIBUTE);


        scene_mvpMatrixUniform = GLES20.glGetUniformLocation(mActiveProgram, RenderConstants.MVP_MATRIX_UNIFORM);//
        scene_mvMatrixUniform = GLES20.glGetUniformLocation(mActiveProgram, RenderConstants.MV_MATRIX_UNIFORM);//
        scene_lightPosUniform = GLES20.glGetUniformLocation(mActiveProgram, RenderConstants.LIGHT_POSITION_UNIFORM);//
        scene_textureShadowUniform = GLES20.glGetUniformLocation(mActiveProgram, RenderConstants.SHADOW_TEXTURE);
        scene_positionAttribute = GLES20.glGetAttribLocation(mActiveProgram, RenderConstants.POSITION_ATTRIBUTE);
        scene_normalAttribute = GLES20.glGetAttribLocation(mActiveProgram, RenderConstants.NORMAL_ATTRIBUTE);//
        scene_textureCoord = GLES20.glGetAttribLocation(mActiveProgram, RenderConstants.TEX_COORDINATE);
        scene_textureUniform = GLES20.glGetAttribLocation(mActiveProgram, RenderConstants.TEXTURE_UNIFORM);

        //shadow handles
		int shadowMapProgram = mDepthMapProgram.getProgram();
		shadow_mvpMatrixUniform = GLES20.glGetUniformLocation(shadowMapProgram, RenderConstants.MVP_MATRIX_UNIFORM);
		shadow_positionAttribute = GLES20.glGetAttribLocation(shadowMapProgram, RenderConstants.SHADOW_POSITION_ATTRIBUTE);


		//--------------- calc values common for both renderers
		
		// light rotates around Y axis in every 12 seconds
        long elapsedMilliSec = SystemClock.elapsedRealtime();
        long rotationCounter = elapsedMilliSec % 12000L;
        
        float lightRotationDegree = (360.0f / 12000.0f) * ((int)rotationCounter);
        //lightRotationDegree = 240;
        
        float[] rotationMatrix = new float[16];
        
        Matrix.setIdentityM(rotationMatrix, 0);
        Matrix.rotateM(rotationMatrix, 0, lightRotationDegree, 0.0f, 1.0f, 0.0f);

        Matrix.multiplyMV(mActualLightPosition, 0, rotationMatrix, 0, mLightPosModel, 0);
	
        Matrix.setIdentityM(mModelMatrix, 0);
        
        //Set view matrix from light source position
        Matrix.setLookAtM(mLightViewMatrix, 0,
        					//lightX, lightY, lightZ, 
        					mActualLightPosition[0], mActualLightPosition[1], mActualLightPosition[2],
        					//lookX, lookY, lookZ,
        					//look in direction -y
        					mActualLightPosition[0], -mActualLightPosition[1], mActualLightPosition[2],
        					//upX, upY, upZ
        					//up vector in the direction of axisY
        					-mActualLightPosition[0], 0, -mActualLightPosition[2]);



        //------------------------- render depth map --------------------------

        // Cull front faces for shadow generation to avoid self shadowing
     	GLES20.glCullFace(GLES20.GL_FRONT);

     	renderShadowMap();
     	
        //------------------------- render scene ------------------------------


     	// Cull back faces for normal render
     	GLES20.glCullFace(GLES20.GL_BACK);
     	renderScene();

        // Print openGL errors to console
        int debugInfo = GLES20.glGetError();
		
		if (debugInfo != GLES20.GL_NO_ERROR) {
			String msg = "OpenGL error: " + debugInfo;
			Log.w(TAG, msg);
		}
        mMillAngle += mMillSpeed;
		
    }

    private void renderShadowMap() {
    	// bind the generated framebuffer
    	GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, fboId[0]);
    	
		GLES20.glViewport(0, 0, mShadowMapWidth, mShadowMapHeight);
		
		// Clear color and buffers
		GLES20.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);
		
		// Start using the shader
		GLES20.glUseProgram(mDepthMapProgram.getProgram());

		float[] tempResultMatrix = new float[16];
		
		// Calculate matrices for standing objects

		// View matrix * Model matrix value is stored
		Matrix.multiplyMM(mLightMvpMatrix_staticShapes, 0, mLightViewMatrix, 0, mModelMatrix, 0);

		// Model * view * projection matrix stored and copied for use at rendering from camera point of view
		Matrix.multiplyMM(tempResultMatrix, 0, mLightProjectionMatrix, 0, mLightMvpMatrix_staticShapes, 0);
		System.arraycopy(tempResultMatrix, 0, mLightMvpMatrix_staticShapes, 0, 16);
		
		// Pass in the combined matrix.
		GLES20.glUniformMatrix4fv(shadow_mvpMatrixUniform, 1, false, mLightMvpMatrix_staticShapes, 0);

		// Render all stationary shapes on scene
        mBuilding.render(shadow_positionAttribute, 0, 0, 0, true);
        mBuildingLittle.render(shadow_positionAttribute, 0, 0, 0, true);
        mLandLeft.render(shadow_positionAttribute, 0, 0, 0, true);
        mLandRight.render(shadow_positionAttribute, 0, 0, 0, true);
        //mRail.render(shadow_positionAttribute, 0, 0, true);
        mRoof.render(shadow_positionAttribute, 0, 0, 0, true);
        //mWater.render(shadow_positionAttribute, 0, 0, true);



        for (int i = 0; i < 360; i = i + 45) {
            // Calculate matrices for moving objects
            float[] cubeRotationX = new float[16];
            float[] cubeRotationY = new float[16];
            Matrix.setRotateM(cubeRotationX, 0, 90f, -1, 0, 0);
            Matrix.setRotateM(cubeRotationY, 0, i - mMillAngle, 0, 1, 0);
            Matrix.multiplyMM(mCubeRotation, 0, cubeRotationX, 0, cubeRotationY, 0);
            // Rotate the model matrix with current rotation matrix
            Matrix.multiplyMM(tempResultMatrix, 0, mModelMatrix, 0, mCubeRotation, 0);

            // View matrix * Model matrix value is stored
            Matrix.multiplyMM(mLightMvpMatrix_dynamicShapes, 0, mLightViewMatrix, 0, tempResultMatrix, 0);

            // Model * view * projection matrix stored and copied for use at rendering from camera point of view
            Matrix.multiplyMM(tempResultMatrix, 0, mLightProjectionMatrix, 0, mLightMvpMatrix_dynamicShapes, 0);
            System.arraycopy(tempResultMatrix, 0, mLightMvpMatrix_dynamicShapes, 0, 16);

            // Pass in the combined matrix.
            GLES20.glUniformMatrix4fv(shadow_mvpMatrixUniform, 1, false, mLightMvpMatrix_dynamicShapes, 0);
            mRectangles[i / 45].render(shadow_positionAttribute, 0, 0,0,  true);
        }

	}
    
    private void renderScene() {
    	// bind default framebuffer
		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
		
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
		
		GLES20.glUseProgram(mActiveProgram);
		
		GLES20.glViewport(0, 0, mDisplayWidth, mDisplayHeight);

        float[] tempResultMatrix = new float[16];

        //calculate MV matrix
        Matrix.multiplyMM(tempResultMatrix, 0, mViewMatrix, 0, mModelMatrix, 0);
        System.arraycopy(tempResultMatrix, 0, mMVMatrix, 0, 16);
        
        //pass in MV Matrix as uniform
        GLES20.glUniformMatrix4fv(scene_mvMatrixUniform, 1, false, mMVMatrix, 0);
        
        //calculate Normal Matrix as uniform (invert transpose MV)
        Matrix.invertM(tempResultMatrix, 0, mMVMatrix, 0);
        Matrix.transposeM(mNormalMatrix, 0, tempResultMatrix, 0);
        //pass in Normal Matrix as uniform
        GLES20.glUniformMatrix4fv(SHADOWscene_normalMatrixUniform, 1, false, mNormalMatrix, 0);
        
        //calculate MVP matrix
        Matrix.multiplyMM(tempResultMatrix, 0, mProjectionMatrix, 0, mMVMatrix, 0);
        System.arraycopy(tempResultMatrix, 0, mMVPMatrix, 0, 16);
        
        //pass in MVP Matrix as uniform
        GLES20.glUniformMatrix4fv(scene_mvpMatrixUniform, 1, false, mMVPMatrix, 0);
        
        Matrix.multiplyMV(mLightPosInEyeSpace, 0, mViewMatrix, 0, mActualLightPosition, 0);
        //pass in light source position
        GLES20.glUniform3f(scene_lightPosUniform, mLightPosInEyeSpace[0], mLightPosInEyeSpace[1], mLightPosInEyeSpace[2]);
        
        //MVP matrix that was used during depth map render
        GLES20.glUniformMatrix4fv(SHADOWscene_schadowProjMatrixUniform, 1, false, mLightMvpMatrix_staticShapes, 0);

        GLES20.glUniform1f(GLES20.glGetUniformLocation(mActiveProgram, "uHasTexture"), 0.0f);
        mLandLeft.render(scene_positionAttribute, scene_normalAttribute, SHADOWscene_colorAttribute, scene_textureShadowUniform, false);
        mLandRight.render(scene_positionAttribute, scene_normalAttribute, SHADOWscene_colorAttribute, scene_textureShadowUniform, false);
        mBuilding.render(scene_positionAttribute, scene_normalAttribute, SHADOWscene_colorAttribute, scene_textureShadowUniform, false);

        GLES20.glUniform1f(GLES20.glGetUniformLocation(mActiveProgram, "uHasTexture"), 1.0f);
        mBuildingLittle.render(scene_positionAttribute, scene_normalAttribute, scene_textureCoord, scene_textureUniform, false);
        mRail.render(scene_positionAttribute, scene_normalAttribute, SHADOWscene_colorAttribute, scene_textureUniform, false);
        mRoof.render(scene_positionAttribute, scene_normalAttribute, scene_textureCoord,  scene_textureUniform, false);
        mWater.render(scene_positionAttribute, scene_normalAttribute, scene_textureCoord, scene_textureUniform, false, true);

        drawMill(tempResultMatrix);

        drawFlow(tempResultMatrix);

	}

    private void drawMill(float[] tempResultMatrix) {
        for (int i = 0; i < 360; i = i + 45) {
            float[] cubeRotationX = new float[16];
            float[] cubeRotationY = new float[16];
            Matrix.setRotateM(cubeRotationX, 0, 90f, -1, 0, 0);
            Matrix.setRotateM(cubeRotationY, 0, i - mMillAngle, 0, 1, 0);
            Matrix.multiplyMM(mCubeRotation, 0, cubeRotationX, 0, cubeRotationY, 0);
            // Rotate the model matrix with current rotation matrix
            Matrix.multiplyMM(tempResultMatrix, 0, mModelMatrix, 0, mCubeRotation, 0);

            //calculate MV matrix
            Matrix.multiplyMM(tempResultMatrix, 0, mViewMatrix, 0, tempResultMatrix, 0);
            System.arraycopy(tempResultMatrix, 0, mMVMatrix, 0, 16);

            //pass in MV Matrix as uniform
            GLES20.glUniformMatrix4fv(scene_mvMatrixUniform, 1, false, mMVMatrix, 0);

            //calculate Normal Matrix as uniform (invert transpose MV)
            Matrix.invertM(tempResultMatrix, 0, mMVMatrix, 0);
            Matrix.transposeM(mNormalMatrix, 0, tempResultMatrix, 0);

            //pass in Normal Matrix as uniform
            GLES20.glUniformMatrix4fv(SHADOWscene_normalMatrixUniform, 1, false, mNormalMatrix, 0);

            //calculate MVP matrix
            Matrix.multiplyMM(tempResultMatrix, 0, mProjectionMatrix, 0, mMVMatrix, 0);
            System.arraycopy(tempResultMatrix, 0, mMVPMatrix, 0, 16);

            //pass in MVP Matrix as uniform
            GLES20.glUniformMatrix4fv(scene_mvpMatrixUniform, 1, false, mMVPMatrix, 0);

            //MVP matrix that was used during depth map render
            GLES20.glUniformMatrix4fv(SHADOWscene_schadowProjMatrixUniform, 1, false, mLightMvpMatrix_dynamicShapes, 0);
            mRectangles[i / 45].render(scene_positionAttribute, scene_normalAttribute, scene_textureCoord, scene_textureUniform, false);

        }
    }

    private void drawFlow(float[] tempResultMatrix) {
        for(int i = 0; i < FLOW_COUNT; i++) {
            float[] transMatrix = new float[16];
            Matrix.setIdentityM(transMatrix, 0);
            Matrix.translateM(transMatrix, 0, mFlows[i].getTranslation(), 0, 0);

            Matrix.multiplyMM(tempResultMatrix, 0, mModelMatrix, 0, transMatrix, 0);

            //calculate MV matrix
            Matrix.multiplyMM(tempResultMatrix, 0, mViewMatrix, 0, tempResultMatrix, 0);
            System.arraycopy(tempResultMatrix, 0, mMVMatrix, 0, 16);

            //pass in MV Matrix as uniform
            GLES20.glUniformMatrix4fv(scene_mvMatrixUniform, 1, false, mMVMatrix, 0);

            //calculate Normal Matrix as uniform (invert transpose MV)
            Matrix.invertM(tempResultMatrix, 0, mMVMatrix, 0);
            Matrix.transposeM(mNormalMatrix, 0, tempResultMatrix, 0);

            //pass in Normal Matrix as uniform
            GLES20.glUniformMatrix4fv(SHADOWscene_normalMatrixUniform, 1, false, mNormalMatrix, 0);

            //calculate MVP matrix
            Matrix.multiplyMM(tempResultMatrix, 0, mProjectionMatrix, 0, mMVMatrix, 0);
            System.arraycopy(tempResultMatrix, 0, mMVPMatrix, 0, 16);

            //pass in MVP Matrix as uniform
            GLES20.glUniformMatrix4fv(scene_mvpMatrixUniform, 1, false, mMVPMatrix, 0);

            //MVP matrix that was used during depth map render
            GLES20.glUniformMatrix4fv(SHADOWscene_schadowProjMatrixUniform, 1, false, mLightMvpMatrix_dynamicShapes, 0);

            mFlows[i].getObject().render(scene_positionAttribute, scene_normalAttribute, SHADOWscene_colorAttribute, scene_textureUniform, false, true);
        }
    }

    /**
     * Changes render program after changes in menu


    /**
     * Returns the X rotation angle of the cube.
     *
     * @return - A float representing the rotation angle.
     */
    public float getRotationX() {
        return mRotationX;
    }

    /**
     * Sets the X rotation angle of the cube.
     */
    public void setRotationX(float rotationX) {
        mRotationX = rotationX;
    }
    
    /**
     * Returns the Y rotation angle of the cube.
     *
     * @return - A float representing the rotation angle.
     */
    public float getRotationY() {
        return mRotationY;
    }

    /**
     * Sets the Y rotation angle of the cube.
     */
    public void setRotationY(float rotationY) {
        mRotationY = rotationY;
    }

}
