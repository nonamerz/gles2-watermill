package com.projectI.openglshadowdemo;

import android.opengl.GLES20;

import com.projectI.openglshadowdemo.common.RenderConstants;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Simple cube in openGL
 *
 */
public class Triangle {
	private final FloatBuffer cubePosition;
	private final FloatBuffer cubeNormal;
	private final FloatBuffer cubeColor;
    private final float[] mPosition;
    private final int[] mRenderTextureId;

    public float[] getPosition() {
        return mPosition;
    }
    // 6 sides * 2 triangles * 3 vertices * 3 coordinates



    final float[] cubeNormalData =
            {
                    // Front face
                    0.0f, 0.0f, 1.0f,
                    0.0f, 0.0f, 1.0f,
                    0.0f, 0.0f, 1.0f,
                    0.0f, 0.0f, 1.0f,
                    0.0f, 0.0f, 1.0f,
                    0.0f, 0.0f, 1.0f,

                    // Right face
                    1.0f, 0.0f, 0.0f,
                    1.0f, 0.0f, 0.0f,
                    1.0f, 0.0f, 0.0f,
                    1.0f, 0.0f, 0.0f,
                    1.0f, 0.0f, 0.0f,
                    1.0f, 0.0f, 0.0f,

                    // Back face
                    0.0f, 0.0f, -1.0f,
                    0.0f, 0.0f, -1.0f,
                    0.0f, 0.0f, -1.0f,
                    0.0f, 0.0f, -1.0f,
                    0.0f, 0.0f, -1.0f,
                    0.0f, 0.0f, -1.0f,

                    // Left face
                    -1.0f, 0.0f, 0.0f,
                    -1.0f, 0.0f, 0.0f,
                    -1.0f, 0.0f, 0.0f,
                    -1.0f, 0.0f, 0.0f,
                    -1.0f, 0.0f, 0.0f,
                    -1.0f, 0.0f, 0.0f,

                    // Top face
                    0.0f, 1.0f, 0.0f,
                    0.0f, 1.0f, 0.0f,
                    0.0f, 1.0f, 0.0f,
                    0.0f, 1.0f, 0.0f,
                    0.0f, 1.0f, 0.0f,
                    0.0f, 1.0f, 0.0f,

                    // Bottom face
                    0.0f, -1.0f, 0.0f,
                    0.0f, -1.0f, 0.0f,
                    0.0f, -1.0f, 0.0f,
                    0.0f, -1.0f, 0.0f,
                    0.0f, -1.0f, 0.0f,
                    0.0f, -1.0f, 0.0f
            };

	//size of color data array: 6 sides * 2 triangles * 3 points * 4 color (RGBA) values
	float[] cubeColorData = new float[12*3*4];

	/**
	 * Create a new cube with specified center position, size and color
	 */
	public Triangle(float x, float y, float z, float zOffset, float[] position, float[] color) {
		//set color data
		for (int v = 0; v < 12*3 ; v++){
			cubeColorData[4*v+0] = color[0];
			cubeColorData[4*v+1] = color[1];
			cubeColorData[4*v+2] = color[2];
			cubeColorData[4*v+3] = color[3];
		}

        float[] cubePositionData = getPositionData(x, y, z, zOffset);
        for (int j = 0; j < 36; j++) {
            cubePositionData[3*j] = cubePositionData[3*j] + position[0];
            cubePositionData[3*j + 1] = cubePositionData[3*j + 1] + position[1];
            cubePositionData[3*j + 2] = cubePositionData[3*j + 2] + position[2];
        }
        mPosition = position;
        mRenderTextureId = new int[1];
        GLES20.glGenTextures(1, mRenderTextureId, 0);

        // Initialize the buffers.
		ByteBuffer bPos = ByteBuffer.allocateDirect(cubePositionData.length * RenderConstants.FLOAT_SIZE_IN_BYTES);
		bPos.order(ByteOrder.nativeOrder());
		cubePosition = bPos.asFloatBuffer();
		
		ByteBuffer bNormal = ByteBuffer.allocateDirect(cubeNormalData.length * RenderConstants.FLOAT_SIZE_IN_BYTES);
		bNormal.order(ByteOrder.nativeOrder());
		cubeNormal = bNormal.asFloatBuffer();
		
		ByteBuffer bColor = ByteBuffer.allocateDirect(cubeColorData.length * RenderConstants.FLOAT_SIZE_IN_BYTES);
		bColor.order(ByteOrder.nativeOrder());
		cubeColor = bColor.asFloatBuffer();
					
		cubePosition.put(cubePositionData).position(0);
		cubeNormal.put(cubeNormalData).position(0);
		cubeColor.put(cubeColorData).position(0);
	}

    private float[] getPositionData(float x, float y, float z, float offset) {
        return new float[]
                {
                        // In OpenGL counter-clockwise winding is default. This means that when we look at a triangle,
                        // if the points are counter-clockwise we are looking at the "front". If not we are looking at
                        // the back. OpenGL has an optimization where all back-facing triangles are culled, since they
                        // usually represent the backside of an object and aren't visible anyways.

                        // Front face
                        x/2,	y,		z + offset,		//h11
                        0.0f,	0.0f,	z + offset,		//001
                        x/2,	y,		z + offset,		//h11
                        0.0f,	0.0f,	z + offset,		//001
                        x,		0.0f,	z + offset,		//101
                        x/2,	y,		z + offset,		//h11

                        // Right face
                        x/2,	y,		z + offset,		//h11
                        x,		0.0f,	z + offset,		//101
                        x/2,	y,		0 + offset,		//h10
                        x,		0.0f,	z + offset,		//101
                        x,		0.0f,	0 + offset,		//100
                        x/2,	y,		0 + offset,		//h10

                        // Back face
                        x/2,	y,		0 + offset,		//h10
                        x,		0.0f,	0 + offset,		//100
                        x/2,	y,		0 + offset,		//h10
                        x,		0.0f,	0 + offset,		//100
                        0.0f,	0.0f,	0 + offset,		//000
                        x/2,	y,		0 + offset,		//h10

                        // Left face
                        x/2,	y,		0 + offset,		//h10
                        0.0f,	0.0f,	0 + offset,		//000
                        x/2,	y,		z + offset,		//h11
                        0.0f,	0.0f,	0 + offset,		//000
                        0.0f,	0.0f,	z + offset,		//001
                        x/2,	y,		z + offset,		//h11

                        // Top face
                        x/2,	y,		0 + offset,		//h10
                        x/2,	y,		z + offset,		//h11
                        x/2,	y,		0 + offset,		//h10
                        x/2,	y,		z + offset,		//h11
                        x/2,	y,		z + offset,		//h11
                        x/2,	y,		0 + offset,		//h10

                        // Bottom face
                        x/2,	y,		0 + offset,		//h10
                        x,		0.0f,	z + offset,		//101
                        0.0f,	0.0f,	0 + offset,		//000
                        x,		0.0f,	z + offset,		//101
                        0.0f,	0.0f,	z + offset,		//001
                        0.0f,	0.0f,	0 + offset,		//000
                };
    }

    public void render(int positionAttribute, int normalAttribute, int colorAttribute, int textureUniform, boolean onlyPosition) {

        render(positionAttribute, normalAttribute, colorAttribute, textureUniform, onlyPosition, false);
    }
    public void render(int positionAttribute, int normalAttribute, int colorAttribute, int textureUniform, boolean onlyPosition, boolean isTransparent) {

        // Pass in the position information
        cubePosition.position(0);
        GLES20.glVertexAttribPointer(positionAttribute, 3, GLES20.GL_FLOAT, false, 0, cubePosition);

        GLES20.glEnableVertexAttribArray(positionAttribute);


        if (!onlyPosition)
        {
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mRenderTextureId[0]);
            GLES20.glUniform1i(textureUniform, 0);

            // Pass in the normal information
            cubeNormal.position(0);
            GLES20.glVertexAttribPointer(normalAttribute, 3, GLES20.GL_FLOAT, false,
                    0, cubeNormal);

            GLES20.glEnableVertexAttribArray(normalAttribute);

            // Pass in the color information
            cubeColor.position(0);
            GLES20.glVertexAttribPointer(colorAttribute, 4, GLES20.GL_FLOAT, false, 0, cubeColor);
            GLES20.glEnableVertexAttribArray(colorAttribute);

        }
        if(isTransparent) {
            // Draw the cube.
            GLES20.glDepthMask(false);
            GLES20.glEnable(GLES20.GL_BLEND);
            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
            GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 36);
            GLES20.glDisable(GLES20.GL_BLEND);
            GLES20.glDepthMask(true);
        } else {
            GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 36);
        }
    }
}